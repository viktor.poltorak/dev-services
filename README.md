#Development services container

## services

Control panel:

http://localhost:5555

### PostgreSQL - database 

There is 2 option

 clean postgres
  
 postgres with postgis
 
Check the docker-compose.yml and uncomment needed db

if needed it's possible to have serveral instanse of db


  * port: 5432
  * user: app_user
  * default db: app_db
  * default pass: app_pass
  
### Mailhog - mail catcher app
 * UI url: http://localhost:8025/
 * sendmail url: http://localhost:1025/
 
### Redis
 * port: 6379

### Rabbtmq: 
*  ports:
    - 15672:15672
    - 5672:5672

### Memcached
 * port: 11211

# Run
docker-compose up -d

Open http://localhost:5555

