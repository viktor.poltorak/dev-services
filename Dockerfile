FROM ubuntu:18.04
ENV ENV FLASK_ENV=production

MAINTAINER Viktor Poltorak <viktor.poltorak@mpom.ch>

RUN apt-get clean && \
    apt-get update && \
    apt-get install -y \
    python3 \
    python3-pip \
    nano

# 5000 - service default port
EXPOSE 5555

WORKDIR /

COPY app.py /app.py
COPY requirements.txt /requirements.txt
COPY ./entrypoint.sh /
RUN pip3 install -r requirements.txt

CMD ["/entrypoint.sh"]
