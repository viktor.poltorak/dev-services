# -*- coding: utf-8 -*-
from flask import Flask, Markup, render_template_string
import cherrypy
from paste.translogger import TransLogger
import socket

app = Flask(__name__)


def check_server(address, port):
    # Create a TCP socket
    s = socket.socket()
    print("Attempting to connect to %s on port %s" % (address, port))
    try:
        s.connect((address, port))
        print("Connected to %s on port %s" % (address, port))
        return True
    except Exception as e:
        print("Connection to %s on port %s failed: %s" % (address, port, e))
        return False
    finally:
        s.close()


services = [
    {'name': 'PostgreSQL Updated', 'host': 'db', 'port': 5432, 'link': False},
    {'name': 'Adminer', 'host': 'adminer', 'port': 8080, 'link': 'localhost:8050?pgsql=db'},
    {'name': 'RabbitMQ', 'host': 'rabbitmq', 'port': 5672, 'link': 'localhost:15672'},
    {'name': 'MailHog UI ', 'host': 'mail', 'port': 8025, 'link': 'localhost:8025'},
    {'name': 'MailHog Sendmail server ', 'host': 'mail', 'port': 1025, 'link': False},
    {'name': 'Redis', 'host': 'redis', 'port': 6379, 'link': False},
    {'name': 'Memcached', 'host': 'cache', 'port': 11211, 'link': False},
    {'name': 'Clickhouse', 'host': 'clickhouse', 'port': 9000, 'link': 'localhost:8123'},
]

layout = '<html><head><title>Development services</title></head><body>{{ content }}</body></html>'


@app.route("/", methods=['GET'])
def home():
    main_tpl = '''<table><tr><th>Service Name</th><th>Address</th><th>Status</th><th></th></tr>{{ lines }}</table>'''

    line_tpl = '''
    <tr>
    <td>{{ name }}</td>
    <td>{{ address }}</td>
    <td>&nbsp&nbsp&nbsp&nbsp{{ status }}</td>
    <td>
    {% if link %}
    <a href="http://{{link}}" target="_blank">Open</a>
    {% endif %}
    </td>
    </td>
    '''

    lines = ''
    for service in services:
        if check_server(service['host'], service['port']):
            status = '<span style="color:green">OK</span>'
        else:
            status = '<span style="color:red">FAIL</span>'

        lines += render_template_string(line_tpl, name=service['name'],
                                        address=f'localhost:{service["port"]}', status=Markup(status),
                                        link=service['link'])

    body = render_template_string(main_tpl, lines=Markup(lines))
    return render_template_string(layout, content=Markup(body))


def run_server():
    # Enable WSGI access logging via Paste
    app_logged = TransLogger(app)

    # Mount the WSGI callable object (app) on the root directory
    cherrypy.tree.graft(app_logged, '/')

    # Set the configuration of the web server
    cherrypy.config.update({
        'engine.autoreload_on': True,
        'log.screen': True,
        'server.socket_port': 5555,
        'server.socket_host': '0.0.0.0'
    })

    # Start the CherryPy WSGI web server
    cherrypy.engine.start()
    cherrypy.engine.block()


if __name__ == '__main__':
    run_server()
